name := """alpakka-sample"""

version := "1.0"

scalaVersion := "2.11.7"

// Change this to another test framework if you prefer
libraryDependencies += "org.scalatest" %% "scalatest" % "2.2.4" % "test"
libraryDependencies += "com.lightbend.akka" %% "akka-stream-alpakka-amqp" % "0.6"
libraryDependencies += "com.typesafe.akka" %% "akka-actor" % "2.4.17"
libraryDependencies += "com.typesafe.akka" %% "akka-agent" % "2.4.17"
libraryDependencies += "com.typesafe.akka" %% "akka-camel" % "2.4.17"
libraryDependencies += "com.typesafe.akka" %% "akka-cluster" % "2.4.17"
libraryDependencies += "com.typesafe.akka" %% "akka-cluster-metrics" % "2.4.17"
libraryDependencies += "com.typesafe.akka" %% "akka-cluster-sharding" % "2.4.17"
libraryDependencies += "com.typesafe.akka" %% "akka-cluster-tools" % "2.4.17"
libraryDependencies += "com.typesafe.akka" %% "akka-contrib" % "2.4.17"
libraryDependencies += "com.typesafe.akka" %% "akka-multi-node-testkit" % "2.4.17"
libraryDependencies += "com.typesafe.akka" %% "akka-osgi" % "2.4.17"
libraryDependencies += "com.typesafe.akka" %% "akka-persistence" % "2.4.17"
libraryDependencies += "com.typesafe.akka" %% "akka-persistence-tck" % "2.4.17"
libraryDependencies += "com.typesafe.akka" %% "akka-remote" % "2.4.17"
libraryDependencies += "com.typesafe.akka" %% "akka-slf4j" % "2.4.17"
libraryDependencies += "com.typesafe.akka" %% "akka-stream" % "2.4.17"
libraryDependencies += "com.typesafe.akka" %% "akka-stream-testkit" % "2.4.17"
libraryDependencies += "com.typesafe.akka" %% "akka-testkit" % "2.4.17"

// Uncomment to use Akka
//libraryDependencies += "com.typesafe.akka" %% "akka-actor" % "2.3.11"

