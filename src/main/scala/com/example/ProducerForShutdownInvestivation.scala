package com.example

import akka.actor.ActorSystem
import akka.stream.{ActorMaterializer, KillSwitches}
import akka.stream.alpakka.amqp.scaladsl.AmqpSink
import akka.stream.alpakka.amqp.{AmqpSinkSettings, DefaultAmqpConnection, QueueDeclaration}
import akka.stream.scaladsl.{Flow, Keep, Source}
import akka.util.ByteString

import scala.concurrent.Await
import scala.concurrent.duration.Duration

/**
  * Created by kenichihakiri on 2017/03/02.
  */
object ProducerForShutdownInvestivation extends App with CommonSettings {
    val source      = Source(Stream.continually("hello"))
    val convertFlow = Flow[String].map(ByteString(_))
    val amqpSink    = AmqpSink.simple(
      AmqpSinkSettings(DefaultAmqpConnection)
        .withRoutingKey(queueName)
        .withDeclarations(queueDeclaration)
    )

    val graph = source.via(convertFlow)
      .viaMat(KillSwitches.single)(Keep.right)
      .toMat(amqpSink)(Keep.both)

    val (killSwitch, notUsed) = graph.run()

    println("start publishing")
    Thread.sleep(1000 * 5)
    println("stop publishing")

    killSwitch.shutdown()
    system.terminate
    Await.result(system.whenTerminated, Duration.Inf)

    println("terminated")
}
