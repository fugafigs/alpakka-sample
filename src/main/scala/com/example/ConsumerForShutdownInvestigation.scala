package com.example

import akka.actor.ActorSystem
import akka.stream.{ActorMaterializer, KillSwitches}
import akka.stream.alpakka.amqp._
import akka.stream.alpakka.amqp.scaladsl.AmqpSource
import akka.stream.scaladsl.{Flow, Keep, Sink}
import akka.util.ByteString
import com.example.CommonSettings

import scala.concurrent.Await
import scala.concurrent.duration.Duration

object ConsumerForShutdownInvestigation extends App with CommonSettings {
  val source = AmqpSource(
    NamedQueueSourceSettings(
      connectionSettings = DefaultAmqpConnection,
      queue = queueName
    ).withDeclarations(exchangeDeclaration, queueDeclaration,bindingDeclaration),
    bufferSize = 1
  )

  val fetchedFlow = Flow[IncomingMessage].map { incoming =>
    println(s"fetched ${incoming.bytes.utf8String}")
    incoming
  }

  val sink = Sink.foreach { incoming : IncomingMessage =>
    println(incoming.bytes.utf8String)
  }

  val graph = source
    .via(fetchedFlow)
    .viaMat(KillSwitches.single)(Keep.right)
    .toMat(sink)(Keep.both)

  val (killSwitch, notUsed) = graph.run()

  sys.addShutdownHook {
    println("### shutdown requested")
    killSwitch.shutdown()
    Thread.sleep(1000 * 3)

    system.terminate()
    Await.result(system.whenTerminated, Duration.Inf)
    println("### terminated")
  }
}