package com.example

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.alpakka.amqp.{BindingDeclaration, ExchangeDeclaration, QueueDeclaration}

trait CommonSettings {
  implicit val system       = ActorSystem("investigation")
  implicit val materializer = ActorMaterializer()

  val queueName           = "investigation-queue"
  val exchangeDeclaration = ExchangeDeclaration(name = queueName, exchangeType = "direct")
  val queueDeclaration    = QueueDeclaration(name = queueName)
  val bindingDeclaration  = BindingDeclaration(queue = queueName, exchange = queueName, routingKey = Some(queueName))
}
